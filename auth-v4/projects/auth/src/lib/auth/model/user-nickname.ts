import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, UniqueIdUtil } from '@ngcore/core';
import { BaseModel } from '@ngcore/base';


/**
 * User - Nickname association.
 * Note that (unique) "nickname" acts as an id for local user accounts (possibly, across different users).
 * This table is stored only locally and is not synch'ed with cloud db. (More like a cookie.)
 *     Note: only the nicknames of authenticatable (associated with AuthIdentity) are stored. 
 *     Local profile user nickname is stored in UserProfile.
 * The purpose of this model is to "bootstrap" login process.
 *     e.g., by finding a user account from a nickname. 
 */
export class UserNickname extends BaseModel {

//   // Note; nickname is essentially the primary key.
//   static getDocId(nickname: string, accountId: string): string {
//     return nickname + ':' + accountId;
//   }
//   getDocId(): string {
//     return this.nickname + ':' + this.accountId;
//   }
//   setDocId(_id: string) {
//     let idx = -1;
//     if (_id) {
//       idx = _id.indexOf(':');
//     }
//     if (idx == -1) {
//       this.nickname = _id;
//       this.accountId = null;   // ????
//     } else {
//       this.nickname = _id.substring(0, idx);
//       this.accountId = (_id.length > idx + 1) ? _id.substring(idx + 1) : "";  // ???
//     }
//   }


  // TBD
  // Nickname cannot be docId since it can change.
  // docId should be immutable...

  // Note: nickname is the primary key.
  //  --> this is a problem. docId should be immutable.
  // static getDocId(nickname: string): string {
  //   return nickname;
  // }
  // getDocId(): string {
  //   return this.nickname;
  // }
  // setDocId(_id: string) {
  //   this.nickname = _id;
  // }


  // TBD:
  //  --> This should be set in UserAccount...
  // // If true, the account will be assumed to be logged on once selected by the user.
  // // It can be set to true, only if any identity of the account has been validated
  // //    (at least at the time when this switch is turned on). 
  // isKeepLoggedOnSet: boolean = false;
  
  setNickname(_nickname: string) { this.nickname = _nickname; this.isDirty = true; }
  setAccountId(_accountId: string) { this.accountId = _accountId; this.isDirty = true; }
  setAuthId(_authId: string) { this.authId = _authId; this.isDirty = true; }

  // TBD:
  // How to make nickname not-settable???
  // For now, since we are using nickname as docId, we should not allow changing it (once saved)....
  // ....
  constructor(private _uid: (string | null) = null, public nickname: (string | null) = null, public accountId: (string | null) = null, public authId: (string | null) = null) {
    super(null, _uid);
  }


  toString(): string {
    return super.toString()
      + '; userId = ' + this.userId
      + '; nickname = ' + this.nickname
      + '; accountId = ' + this.accountId
      + '; authId = ' + this.authId;
      // + '; isKeepLoggedOnSet = ' + this.isKeepLoggedOnSet;
  }


  clone(): UserNickname {
    let cloned = Object.assign(new UserNickname(), this) as UserNickname;
    return cloned;
  }
  static clone(obj: any): UserNickname {
    let cloned = Object.assign(new UserNickname(), obj) as UserNickname;
    return cloned;
  }

  copy(): UserNickname {
    let obj = this.clone();
    // obj.id = RandomIdUtil.id();
    obj.id = UniqueIdUtil.id();
    // userId ????
    obj.resetCreatedTime();
    // obj.isKeepLoggedOnSet = false;   // ???
    return obj;
  }
}
