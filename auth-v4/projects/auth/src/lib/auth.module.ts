import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';

// import { SampleComponent } from './src/sample.component';
// import { SampleDirective } from './src/sample.directive';
// import { SamplePipe } from './src/sample.pipe';
// import { SampleService } from './src/sample.service';

// import { UserNickname } from './src/auth/model/user-nickname';
// import { UserAccount } from './src/auth/model/user-account';
// import { LoginOption } from './src/auth/model/login-option';

// import { UserNicknamePouchRepo } from './src/auth/pouch/user-nickname-pouch-repo';
// import { UserAccountPouchRepo } from './src/auth/pouch/user-account-pouch-repo';
// import { LoginOptionPouchRepo } from './src/auth/pouch/login-option-pouch-repo';

// import { UserNicknameHelper } from './src/auth/providers/user-nickname-helper';
// import { UserAccountHelper } from './src/auth/providers/user-account-helper';
// import { LoginOptionHelper } from './src/auth/providers/login-option-helper';

// import { AuthDatabaseService } from './src/services/auth-database-service';
// import { AuthManagerService } from './src/services/auth-manager-service';


// // TBD:
// var dbNamePrefix = "ngcore-auth-db1";
// // ...
// export function provideUserNicknamePouchRepo(): UserNicknamePouchRepo {
//   return new UserNicknamePouchRepo(dbNamePrefix + '-' + UserNickname.name.toLowerCase());
// }
// export function provideUserAccountPouchRepo(): UserAccountPouchRepo {
//   return new UserAccountPouchRepo(dbNamePrefix + '-' + UserAccount.name.toLowerCase());
// }
// export function provideLoginOptionPouchRepo(): LoginOptionPouchRepo {
//   return new LoginOptionPouchRepo(dbNamePrefix + '-' + LoginOption.name.toLowerCase());
// }


@NgModule({
  imports: [
    CommonModule,
    // BrowserModule,
    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot()
  ],
  declarations: [
    // SampleComponent,
    // SampleDirective,
    // SamplePipe
  ],
  exports: [
    // SampleComponent,
    // SampleDirective,
    // SamplePipe
  ]
})
export class NgCoreAuthModule {
  static forRoot(): ModuleWithProviders<NgCoreAuthModule> {
    return {
      ngModule: NgCoreAuthModule,
      providers: [

        // { provide: UserNicknamePouchRepo, useFactory: provideUserNicknamePouchRepo },
        // { provide: UserAccountPouchRepo, useFactory: provideUserAccountPouchRepo },
        // { provide: LoginOptionPouchRepo, useFactory: provideLoginOptionPouchRepo },
        // UserNicknameHelper,
        // UserAccountHelper,
        // LoginOptionHelper,

        // AuthDatabaseService,
        // AuthManagerService
        
      ]
    };
  }
}
